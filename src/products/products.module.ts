import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from './entities/product.entity';
import { ProductImagesEntity } from './entities/productImage.entity';

@Module({
  imports : [
    TypeOrmModule.forFeature([ProductEntity,ProductImagesEntity])
  ],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
