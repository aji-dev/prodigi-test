import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsString, IsNotEmpty, IsOptional, IsNumber } from 'class-validator';


export class UpdateProductDto {
    @ApiProperty()
    @IsNotEmpty()
    title : string

    @ApiProperty()
    @IsNotEmpty()
    description : string

    @ApiProperty()
    @IsNotEmpty()
    brand : string

    @ApiProperty()
    @IsNotEmpty()
    price : number

    @ApiProperty()
    @IsNotEmpty()
    discount : number
}
