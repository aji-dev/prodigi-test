import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsDate, IsString, IsNotEmpty, IsOptional, IsNumber } from 'class-validator';

export class FindProductsDto {
    @ApiProperty()
    @IsNotEmpty()
    page : number

    @ApiProperty()
    @IsNotEmpty()
    perpage : number

    @ApiPropertyOptional()
    @IsOptional()
    keyword : string
}
