import { IsDate, IsString, IsNotEmpty, IsOptional, IsNumber } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';


export class CreateProductDto {
    @ApiProperty()
    @IsNotEmpty()
    title : string

    @ApiProperty()
    @IsNotEmpty()
    description : string

    @ApiProperty()
    @IsNotEmpty()
    brand : string

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    price : number

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    discount : number
    
    @ApiPropertyOptional({type : [String]})
    @IsOptional()
    images : string[]
}
