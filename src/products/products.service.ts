import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductEntity } from './entities/product.entity';
import { ProductImagesEntity } from './entities/productImage.entity';
import { createQueryBuilder, EntityManager, getConnection, MoreThan,Repository,Connection, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { buffer } from 'stream/consumers';
const oss = require('ali-oss')
const co = require('co')



@Injectable()
export class ProductsService {
  constructor(
    private connection : Connection,
    @InjectRepository(ProductEntity) private productEntity : Repository<ProductEntity>,
    @InjectRepository(ProductImagesEntity) private productImagesEntity : Repository<ProductImagesEntity> 
    ){}
  async create(createProductDto: CreateProductDto) {
    try{
      const addNewProduct = await this.productEntity.save(createProductDto)
      let productImages = []
  
      await Promise.all(createProductDto.images.map(async valueImages=>{
        let image = {
          id_product : addNewProduct.id,
          image_url : valueImages
        }
        await this.productImagesEntity.save(image)
      }))
  
      let response = {
        response_code : 200,
        message : 'success add new product'
      }
  
      return response

    }catch(error){
      throw new BadRequestException({
        response_code : 400,
        error : error
      })
    }
  }

  async uploadImage(file){
    let client = new oss({
      region: 'oss-ap-southeast-5',
      accessKeyId : process.env.ALI_ACCESS_KEY,
      accessKeySecret : process.env.ALI_SECRET_KEY,
      bucket : 'dev-atk'
    })
    let imageUrlname = 'product-'+Math.floor(new Date().getTime()/1000)+'.png' 
    const resultUpload = await client.put('prodigi/'+imageUrlname,new Buffer(file.buffer))
    if(resultUpload.res.status != 200){
      throw new BadRequestException ({
        response_code : 400,
        message : 'failed to upload file'
      })
    }
    let response = {
      response_code : 200,
      message : 'success upload file',
      url_product : resultUpload.url
    }
  }

  async findProducts(param) {
    let where :any = []
    if(param.keyword){
      where = [
        {title : Like(`%${param.keyword}%`)},
        {brand : Like(`%${param.keyword}%`)}
      ]
    }
    let [dataProducts,countProducts] = await this.productEntity.findAndCount({
      where : where,
      order : {
        created_at : 'ASC'
      },
      skip : (param.page -1) * param.perpage,
      take : param.perpage
    })
    let response = {
      response_code : 200,
      message : 'data has been loaded',
      data : {
        products : dataProducts,
        count : countProducts,
        total_page : Math.ceil(countProducts/param.perpage),
        page : param.page,
        perpage : param.perpage
      }
    
      
    }

    return response

  }

  async update(id: number, updateProductDto) {
    let updateDataProduct = await this.productEntity.update(id,updateProductDto)
    let response = {
      response_code : 200,
      message : `success update product id #${id}`
    }
    return response
  }

}
