import { Controller, Get, Post, Body, Patch, Param, Delete,UsePipes, ValidationPipe, UseInterceptors, UploadedFile, Query, HttpCode } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { FindProductsDto } from './dto/find-products.dto';
import { ApiBody, ApiConsumes, ApiResponse } from '@nestjs/swagger';
@UsePipes(
  new ValidationPipe({
    whitelist : true,
    transform : true 
  })
)
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @ApiResponse({status : 201, description : 'data has been saved to database'})
  @ApiResponse({status : 400, description : 'Bad Request Exception'})
  @ApiResponse({status : 500, description : 'Internal server error'})
  @Post('add-product')
  create(@Body() createProductDto: CreateProductDto) {
    return this.productsService.create(createProductDto);
  }

  @HttpCode(200)
  @Post('upload-image')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description : 'file'
  })
  @ApiResponse({status : 200, description : 'file has been upload on alibaba cloud service object storage, copy paste file url to browser check image file'})
  @ApiResponse({status : 400, description : 'Bad Request Exception'})
  @ApiResponse({status : 500, description : 'Internal server error'})
  uploadImage(@UploadedFile()file){
    return this.productsService.uploadImage(file)
  }

  @ApiResponse({status : 200, description : 'data has been loaded'})
  @ApiResponse({status : 400, description : 'Bad Request Exception'})
  @ApiResponse({status : 500, description : 'Internal server error'})
  @Get()
  findAll(@Query()queryParam : FindProductsDto) {
    return this.productsService.findProducts(queryParam);
  }

  @ApiResponse({status : 200, description : 'data has been updated'})
  @ApiResponse({status : 400, description : 'Bad Request Exception'})
  @ApiResponse({status : 500, description : 'Internal server error'})
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
    return this.productsService.update(+id, updateProductDto);
  }
}
