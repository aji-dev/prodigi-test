import { ProductEntity } from './product.entity';
import {
    Column,
    CreateDateColumn,
    Entity,
    Generated,
    JoinColumn,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
  } from 'typeorm';

@Entity('tb_product_images')
export class ProductImagesEntity {
    @PrimaryGeneratedColumn()
    id : number

    @Column({type : 'int'})
    id_product : number

    @Column({type : 'varchar', length : 400})
    image_url : string

    @ManyToOne(()=>ProductEntity,(productEntity)=>productEntity.productImages)
    @JoinColumn({
        name : 'id_product',
        referencedColumnName : 'id'
    })
    product : ProductEntity[]
}