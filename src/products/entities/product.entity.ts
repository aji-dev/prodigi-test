import { ProductImagesEntity } from './productImage.entity';
import {
    Column,
    CreateDateColumn,
    Entity,
    Generated,
    JoinColumn,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
  } from 'typeorm';

@Entity('tb_products')
export class ProductEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type : 'varchar', length : 100})
    title : string

    @Column({type : 'varchar', length : 500})
    description

    @Column({type : 'varchar', length : 100})
    brand : string

    @Column({ type: 'int' })
    price: number;

    @Column({ type: 'int' })
    discount: number;

    @CreateDateColumn({type : 'timestamptz'})
    created_at : Date

    @UpdateDateColumn({type : 'timestamptz'})
    updated_at : Date

    @OneToMany(()=>ProductImagesEntity,(productImages)=>productImages.product)
    productImages : ProductImagesEntity[]



}
