import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { database } from './config/db.config';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
    ProductsModule,
    ConfigModule.forRoot({isGlobal : true, load : [database]}),
    TypeOrmModule.forRootAsync({
      useFactory : (config : ConfigService) => config.get('db_config'),
      inject : [ConfigService]
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
