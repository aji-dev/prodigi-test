export const database = () => ({
    db_config: {
      type: 'postgres',
      host: process.env.DB_HOST,
      port: 5432,
      username : process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: 'prodigi_test',
      entities: ['./dist/**/entities/*{.ts,.js}'],
      synchronize: true,
      logging: false,
    },
  });
  